const app = require('./config/server');

require('./app/routes/archivos')(app);
const env = require('node-env-file'); // .env file
env(__dirname + '../../.env');


app.listen(app.get('port'), () => {
  console.log('server on port ', app.get('port'));
});
