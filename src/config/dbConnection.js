const mysql = require("mysql");
const n_host = process.env.HOST | 'localhost';
const n_user = process.env.USER | 'root';
const n_password = process.env.PASSWORD | '1234';
const n_database = process.env.DATABASE | 'ejemplo';

module.exports = ()=>{
  return mysql.createConnection({
    host:'localhost',
    user:'root',
    password: '1234',
    database: 'ejemplo'
  });
}
